package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

type sarif struct {
	Schema  string `json:"$schema"`
	Version string `json:"version"`
	Runs    []run  `json:"runs"`
}

type run struct {
	Invocations []invocation `json:"invocations"`
	Results     []result     `json:"results"`
	Tool        struct {
		Driver struct {
			Name            string `json:"name"`
			SemanticVersion string `json:"semanticVersion"`
			Rules           []rule `json:"rules"`
		} `json:"driver"`
	} `json:"tool"`
}

type invocation struct {
	ToolExecutionNotifications []notification `json:"toolExecutionNotifications"`
}

type notification struct {
	Descriptor struct {
		ID string `json:"id"`
	} `json:"descriptor"`
	Level   string `json:"level"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

type rule struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Level string `json:"level"`
	} `json:"defaultConfiguration"`
	Properties ruleProperties `json:"properties"`
	HelpURI    string         `json:"helpUri"`
}

type ruleProperties struct {
	Precision        string   `json:"precision"`
	Tags             []string `json:"tags"`
	SecuritySeverity string   `json:"security-severity"`
}

type result struct {
	RuleID  string `json:"ruleId"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations []struct {
		PhysicalLocation struct {
			ArtifactLocation struct {
				URI       string `json:"uri"`
				URIBaseID string `json:"uriBaseId"`
			} `json:"artifactLocation"`
			Region struct {
				StartLine   int `json:"startLine"`
				StartColumn int `json:"startColumn"`
				EndLine     int `json:"endLine"`
				EndColumn   int `json:"endColumn"`
			} `json:"region"`
		} `json:"physicalLocation"`
	} `json:"locations"`
	Suppressions []struct {
		Kind   string `json:"kind"`             // values= 'inSource', 'external'
		Status string `json:"status,omitempty"` // values= empty,'accepted','underReview','rejected'
		GUID   string `json:"guid,omitempty"`
	} `json:"suppressions,omitempty"`
}

// value is referenced from v15.x report schema, which is in turn referenced from the rails app
// Sources:
// - https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.0.6/src/security-report-format.json#L412
// - https://gitlab.com/gitlab-org/gitlab/-/blob/v15.11.4-ee/ee/app/models/ee/vulnerability.rb?ref_type=tags#L78
const vulnerabilityNameMaxLengthBytes = 255
const vulnerabilityDescriptionMaxLengthBytes = 1048576

// match CWE-XXX only
var cweIDRegex = regexp.MustCompile(`([cC][wW][eE])-(\d{1,4})`)

// match (TYPE)-(ID): (Description)
var tagIDRegex = regexp.MustCompile(`([^-]+)-([^:]+):\s*(.+)`)

// TransformToGLSASTReport will take in a sarif file and output a GitLab SAST Report
func TransformToGLSASTReport(reader io.Reader, rootPath, analyzerID string, scanner Scanner) (*Report, error) {
	s := sarif{}

	jsonBytes, err := readerToBytes(reader)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonBytes, &s)
	if err != nil {
		return nil, err
	}

	if s.Version != "2.1.0" {
		return nil, fmt.Errorf("version for SARIF is %s, but we only support 2.1.0", s.Version)
	}

	newReport := NewReport()
	var allVulns []Vulnerability
	allIds := make([]Identifier, 0, len(s.Runs[0].Tool.Driver.Rules))

	idMap := make(map[string]Identifier)

	// It is generally expected to only have a single run, but best to parse all as it is a collection.
	for _, run := range s.Runs {
		for _, invocation := range run.Invocations {
			for _, notification := range invocation.ToolExecutionNotifications {
				logNotification(notification)
			}
		}

		vulns, ruleMap, err := transformRun(run, rootPath, scanner)
		if err != nil {
			return nil, err
		}

		allVulns = append(allVulns, vulns...)

		if sastFPReductionFeatEnabled() {
			for _, rule := range ruleMap {
				idMap[rule.ID] = primaryIdentifier(rule, scanner)
			}
		}
	}

	for _, id := range idMap {
		allIds = append(allIds, id)
	}

	newReport.Analyzer = analyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = allVulns

	if sastFPReductionFeatEnabled() {
		sort.Slice(allIds, func(i, j int) bool {
			return allIds[i].Value < allIds[j].Value
		})
		newReport.Scan.PrimaryIdentifiers = allIds
	}

	return &newReport, nil
}

func readerToBytes(reader io.Reader) ([]byte, error) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(reader)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// shouldSuppress indicates if the given finding(`result`) should be included
// in the artifact. This is determined based on the `suppressions` property from
// the sarif output file.
func shouldSuppress(r result) bool {
	if len(r.Suppressions) == 0 {
		return false
	}
	for _, sup := range r.Suppressions {
		// if one of the suppressions is under review or rejected, include
		// the finding in the artifact.
		if sup.Status == "underReview" || sup.Status == "rejected" {
			return false
		}
	}
	return true
}

func transformRun(r run, rootPath string, scanner Scanner) ([]Vulnerability, map[string]rule, error) {
	ruleMap := make(map[string]rule)
	for _, rule := range r.Tool.Driver.Rules {
		ruleMap[rule.ID] = rule
	}

	var vulns []Vulnerability
	for _, result := range r.Results {
		if shouldSuppress(result) {
			continue
		}
		for _, location := range result.Locations {
			rule := ruleMap[result.RuleID]

			var description string
			if len(result.Message.Text) > vulnerabilityDescriptionMaxLengthBytes {
				description = result.Message.Text[:vulnerabilityDescriptionMaxLengthBytes]
			} else {
				description = result.Message.Text
			}

			lineEnd := location.PhysicalLocation.Region.EndLine

			vulns = append(vulns, Vulnerability{
				Description: description,
				Category:    CategorySast,
				Name:        name(rule, scanner.Name),
				Severity:    severity(rule),
				Scanner:     &scanner,
				Location: Location{
					File:      removeRootPath(location.PhysicalLocation.ArtifactLocation.URI, rootPath),
					LineStart: location.PhysicalLocation.Region.StartLine,
					LineEnd:   lineEnd,
				},
				Identifiers: identifiers(rule, scanner),
			})
		}
	}
	return vulns, ruleMap, nil
}

// See: https://docs.oasis-open.org/sarif/sarif/v2.1.0/os/sarif-v2.1.0-os.html#_Toc34317855 for more
// information about the level property. The docs say that when level is not defined, then the value is equal
// to warning.
func severity(r rule) SeverityLevel {
	// if `security-severity` is available, parse it instead of the `DefaultConfiguration` property
	if r.Properties.SecuritySeverity != "" {
		parsedSeverity := ParseSeverityLevel(r.Properties.SecuritySeverity)
		securitySeverity := strings.TrimSpace(strings.ToLower(r.Properties.SecuritySeverity))

		if parsedSeverity != SeverityLevelUnknown {
			return parsedSeverity
		}

		// parsed security-severity returns unknown, check if it's actually unknown, or just invalid
		if strings.EqualFold(securitySeverity, "unknown") {
			return SeverityLevelUnknown
		}
	}

	// security-severity was invalid or not provided, fall through to the `DefaultConfiguration` check
	switch r.DefaultConfiguration.Level {
	case "error":
		return SeverityLevelCritical
	case "warning":
		return SeverityLevelMedium
	case "note":
		return SeverityLevelInfo
	case "none":
		// This primarily caters for Kics, which tends to map "info" level rules to the "none" SARIF level. This
		// causes a loss in fidelity when we transform the SARIF report into a GL SAST report, resulting in "info"
		// level findings being reported as "unknown" in the repository's Vulnerability Report.
		//
		// By mapping back to info, we're essentially un-doing the logic in Kics.
		//
		// See https://gitlab.com/gitlab-org/gitlab/-/issues/349141#note_1145792336 for details.
		return SeverityLevelInfo
	default:
		return SeverityLevelMedium
	}
}

func name(r rule, scannerName string) string {
	// KiCS (for now) prefers the fullDescription field as shortDescription is used for the identifier
	if strings.EqualFold(strings.ToLower(scannerName), "kics") {
		if len(r.FullDescription.Text) > vulnerabilityNameMaxLengthBytes {
			return r.FullDescription.Text[:vulnerabilityNameMaxLengthBytes-3] + "..."
		}

		return r.FullDescription.Text
	}

	// prefer shortDescription field over tag: CWE: <text> for semgrep
	if r.ShortDescription.Text != "" && !strings.EqualFold(r.FullDescription.Text, r.ShortDescription.Text) {
		if len(r.ShortDescription.Text) > vulnerabilityNameMaxLengthBytes {
			return r.ShortDescription.Text[:vulnerabilityNameMaxLengthBytes-3] + "..."
		}

		return r.ShortDescription.Text
	}

	for _, tag := range r.Properties.Tags {
		splits := strings.Split(tag, ":")
		if strings.HasPrefix(splits[0], "CWE") && len(splits) > 1 {
			return strings.TrimLeft(splits[1], " ")
		}
	}

	// default to full text description
	if len(r.FullDescription.Text) > vulnerabilityNameMaxLengthBytes {
		return r.FullDescription.Text[:vulnerabilityNameMaxLengthBytes-3] + "..."
	}

	return r.FullDescription.Text
}

func identifiers(r rule, scanner Scanner) []Identifier {
	ids := []Identifier{
		primaryIdentifier(r, scanner),
	}

	for _, tag := range r.Properties.Tags {

		matches := findTagMatches(tag)
		if matches == nil {
			continue
		}

		switch strings.ToLower(matches[1]) {
		case "cwe":
			cweID, err := strconv.Atoi(matches[2])
			if err != nil {
				log.Errorf("Failure to parse CWE ID: %v\n", err)
				continue
			}

			ids = append(ids, CWEIdentifier(cweID))
		case "owasp":
			id := matches[2]
			desc := matches[3]

			components := strings.SplitN(matches[3], "-", 2)
			if len(components) == 2 {
				year := strings.TrimSpace(components[0])
				if _, err := strconv.Atoi(year); err == nil {
					id = id + ":" + year
					desc = strings.TrimSpace(components[1])
				}
			}

			ids = append(ids, OWASPTop10Identifier(id, desc))
		default:
			ids = append(ids, Identifier{
				Type:  IdentifierType(strings.ToLower(matches[1])),
				Name:  matches[3],
				Value: matches[2],
			})
		}
	}

	return ids
}

func findTagMatches(tag string) []string {
	// first try to extract (TAG)-(ID): (Description)
	matches := tagIDRegex.FindStringSubmatch(tag)

	if matches == nil {
		// see if we just have a CWE-(XXX) tag
		matches = cweIDRegex.FindStringSubmatch(tag)
	}

	return matches
}

func primaryIdentifier(r rule, scanner Scanner) Identifier {
	return Identifier{
		Type:  IdentifierType(fmt.Sprintf("%s_id", scanner.ID)),
		Name:  r.Name,
		Value: r.ID,
		URL:   r.HelpURI,
	}
}

func removeRootPath(path, rootPath string) string {
	prefix := strings.TrimSuffix(rootPath, "/") + "/"
	if path[0] != '/' {
		prefix = strings.TrimPrefix(prefix, "/")
	}

	return strings.TrimPrefix(path, prefix)
}

func notificationMsg(notification notification) string {
	return fmt.Sprintf(
		"tool notification %s: %s %s",
		notification.Level,
		notification.Descriptor.ID,
		notification.Message.Text)
}

// https://docs.oasis-open.org/sarif/sarif/v2.1.0/csprd01/sarif-v2.1.0-csprd01.html#_Ref493404972
func logNotification(notification notification) {
	msg := notificationMsg(notification)
	switch notification.Level {
	case "error":
		log.Error(msg)
	case "warning":
		log.Warn(msg)
	case "note":
		log.Info(msg)
	case "none":
		log.Debug(msg)
	default:
		log.Debug(msg)
	}
}

// sastFPReductionFeatEnabled returns true if "sast_fp_reduction" feature flag
// is present in the "GITLAB_FEATURES" env variable
func sastFPReductionFeatEnabled() bool {
	features := os.Getenv("GITLAB_FEATURES")
	return strings.Contains(features, "sast_fp_reduction")
}
